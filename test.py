#!/usr/bin/env python3

import csv
import zlib
from os import listdir
from os.path import isfile, join
import pprint
from ctypes import *


def crc(file_name):
    prev = 0
    for line in open(file_name, "rb"):
        prev = zlib.crc32(line, prev)
    return "%X" % (prev & 0xFFFFFFFF)


dat = {}


def init_dat():
    items_without_crc1 = 0
    rows = 0

    with open('GBgbs.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            if rows == 0:
                print(row.keys())

            rows += 1
            clean_row = {}
            for k, v in row.items():
                if v:
                    if k in ["init_data", "song_data"]:
                        v = v.split()
                        clean_row[k + "_len"] = len(v)
                    elif k in ["track_count", "effect_track_count", "music_track_count"]:
                        v = int(v, 16)
                    clean_row[k] = v

            found = False
            for k in ['crc1', 'crc2', 'crc3', 'crc4']:
                if k in clean_row:
                    dat[clean_row[k]] = clean_row
                    found = True

            if not found:
                items_without_crc1 += 1

    print("items_without_crc1 %d/%d" % (items_without_crc1, rows))


def look_for_rom(file_name):
    crc_ = crc(file_name)
    print("=======")
    print(file_name, crc_)
    print("=======")

    if crc_ in dat.keys():
        datz = dat[crc_]
        pprint.pprint(datz, sort_dicts=False)


def is_in_dat(file_name):
    return


def look_for_roms_in_dat(path):
    roms = [f for f in listdir(path) if isfile(join(path, f))]
    for rom in roms:
        print(rom)
        # look_for_rom(join(path, rom))


def count_roms_in_dat(path):
    roms = [f for f in listdir(path) if isfile(join(path, f))]
    found = 0
    for rom in roms:
        file_name = join(path, rom)
        crc_ = crc(file_name)
        if crc_ in dat.keys():
            found += 1
            rom_dat = dat[crc_]

            found_k = 0
            for k in ['other_start_addr', 'song_tab_start_addr', 'one_byte_change']:
                if k in rom_dat:
                    found_k += 1
                    # print(f"[{k}]: {rom}")
            if found_k == 3:
                print(f"[ALL] {rom}")

    total = len(dat.keys())

    print(f"Found {found}/{total} roms.")

    return found


class Header(Structure):
    _fields_ = [('id', c_char * 3),
                ('version', c_int8),
                ('song_count', c_int8),
                ('start_song', c_int8),
                ('load_addr', c_uint16),
                ('init_addr', c_uint16),
                ('play_addr', c_uint16),
                ('sp', c_uint16),
                ('tma', c_int8),
                ('tac', c_int8),
                ('title', c_char * 32),
                ('author', c_char * 32),
                ('copyright', c_char * 32)]


def print_header(h):
    print("id        ", h.id.decode())
    print("version   ", h.version)
    print("song_count", h.song_count)
    print("start_song", h.start_song)
    print("load_addr ", hex(h.load_addr))
    print("init_addr ", hex(h.init_addr))
    print("play_addr ", hex(h.play_addr))
    print("sp        ", hex(h.sp))
    print("tma       ", h.tma)
    print("tac       ", h.tac)
    print("title     ", h.title.decode('SHIFT_JIS'))
    print("author    ", h.author.decode('SHIFT_JIS'))
    print("copyright ", h.copyright.decode('SHIFT_JIS'))


def parse_gbs(file_name):
    with open(file_name, "rb") as gbs:
        h = Header()
        gbs.readinto(h)
        print("after header", hex(gbs.tell()))
        # print("header size", h.__sizeof__())
        print_header(h)

        rest = gbs.read()
        print("rest", type(rest), len(rest))

        gbs.seek(0x200)
        print("good stuff", hex(gbs.tell()))


def compare():
    with open("gbscomp/tetris.gb", "rb") as gb:
        with open("gbscomp/tetris.gbs", "rb") as gbs:
            # embed()

            gb_addr = 0x6500
            gbs_addr = 0x200

            gbs.seek(gbs_addr)
            gb.seek(gb_addr)
            gbd = gb.read(1)
            gbsd = gbs.read(1)
            print("gb", gbd)
            print("gbs", gbsd)

            while gbd == gbsd and gbd and gbsd:
                gb_addr += 1
                gbs_addr += 1
                gbs.seek(gbs_addr)
                gb.seek(gb_addr)
                gbd = gb.read(1)
                gbsd = gbs.read(1)
                # print("DATA", b2h(gbd), b2h(gbsd))

            print("last gbs", hex(gbs_addr), gbsd)
            print("last gb", hex(gb_addr), gbd)


def count_gb_and_gbc_roms_in_dat():
    found = count_roms_in_dat("../roms/gb")
    found += count_roms_in_dat("../roms/gbc")

    total = len(dat.keys())
    print(f"Total: {found}/{total}")


def print_info():
    # compare()
    init_dat()
    # look_for_roms_in_dat()
    # look_for_rom("../roms/Tetris (World) (Rev A).gb")
    # look_for_rom("roms/Tetris (Japan) (En).gb")
    # parse_gbs("gbscomp/DMG-TRA-0.gbs")
    # parse_gbs("gbscomp/DMG-TRA-1.gbs")
    parse_gbs("../gbscomp/tetris.gbs")

    # count_gb_and_gbc_roms_in_dat()


print_info()
