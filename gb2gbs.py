#!/usr/bin/env python3

import csv
import zlib
import sys
import pprint
from ctypes import *

enable_effect_tracks = True


class Header(Structure):
    _fields_ = [('id', c_char * 3),
                ('version', c_int8),
                ('track_count', c_int8),
                ('start_track', c_int8),
                ('load_addr', c_uint16),
                ('init_addr', c_uint16),
                ('play_addr', c_uint16),
                ('sp', c_uint16),
                ('tma', c_int8),
                ('tac', c_int8),
                ('title', c_char * 32),
                ('author', c_char * 32),
                ('copyright', c_char * 32)]


def init(rom_file_name, gbs_file_fame):
    with open(rom_file_name, "rb") as gb:
        rom = gb.read()

    if len(rom) < 8192:
        print("Warning: ROM size is under 8192 bytes.")
    if len(rom) % 8192 != 0:
        print("Warning: ROM size is not a multiple of 8192 bytes.")

    crc32 = "%X" % zlib.crc32(rom, 0)
    print(f"CRC: {crc32}")

    row = load_csv(crc32)
    pprint.pprint(row, sort_dicts=False)
    write_gbs(rom, row, gbs_file_fame)


def h2b(hex_str, length):
    return int(hex_str, 16).to_bytes(length, byteorder='little')


def write_space_separated_bytes(gbs, string, addr):
    if addr:
        gbs.seek(int(addr, 16))
        for s in string.split(" "):
            gbs.write(h2b(s, 1))


def copy_rom(gbs, row, rom):
    for range_str in row['rom_addr_range'].split(" "):
        r = range_str.split("-")
        froms = r[0].split(":")
        tos = r[1].split(":")

        from_source = int(froms[0], 16)
        from_target = int(froms[1], 16)
        to_source = int(tos[0], 16)
        size = to_source - from_source

        gbs.seek(from_target)
        gbs.write(rom[from_source:from_source+size+1])


def write_byte_map(gbs, row):
    for t in row['byte_map'].split(" "):
        tup = t.split(":")
        gbs.seek(int(tup[0], 16))
        gbs.write(h2b(tup[1], 1))


def write_gbs(rom, row, file_name):
    with open(file_name, 'wb') as gbs:
        h = Header(id=b"GBS",
                   version=int(row['header_version']),
                   track_count=int(row['music_track_count'], 16),
                   start_track=int(row['start_track'], 16),
                   load_addr=int(row['load_addr'], 16),
                   init_addr=int(row['init_addr'], 16),
                   play_addr=int(row['play_addr'], 16),
                   sp=int(row['stack_addr'], 16),
                   tma=int(row['TMA'], 16),
                   tac=int(row['TMC'], 16),
                   title=row['title'].encode("utf-8")[:32],
                   author=b"<?>",
                   copyright=f"{row['year']} {row['developer']}".encode("utf-8")[:32])

        if enable_effect_tracks:
            h.track_count += int(row['effect_track_count'], 16)

        gbs.write(h)

        write_space_separated_bytes(gbs, row['init_data'], row['init_data_addr'])
        write_space_separated_bytes(gbs, row['other_data'], row['other_data_addr'])
        write_space_separated_bytes(gbs, row['track_list_data'], row['track_list_data_addr'])

        copy_rom(gbs, row, rom)

        if row['byte_map']:
            write_byte_map(gbs, row)


def load_csv(crc32):
    with open('GBgbs.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            for k in ['crc1', 'crc2', 'crc3', 'crc4']:
                if row[k] == crc32:
                    return row

    raise Exception("ROM not found in dat.")


def main():
    if len(sys.argv) != 3:
        print("usage: ./GBgbs <in rom> <out gbs>")
    else:
        init(sys.argv[1], sys.argv[2])


main()
